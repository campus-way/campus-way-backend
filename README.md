
# Campus Way Backend

## Campus Way

O Campus Way é um aplicativo que traz maior segurança para os alunos da Unicamp.

Todos que já tiveram a oportunidade de morar por Barão Geraldo sabem o quão perigoso pode ser andar pelo bairro. Especialmente à noite, as ruas de Barão ficam muito vazias e escuras, o que favorece a ocorrência de assaltos ou até estupros. É muito difícil para um aluno da Unicamp ter que passar aperto ou medo toda vez que volta sozinho das aulas à noite. Nesse sentido, o CampusWay fornece a possibilidade de um aluno encontrar outras pessoas que vão fazer um trajeto parecido até o seu destino. Assim, os alunos agora podem andar por Barão em grupos, aumentando o sentimento de segurança no trajeto.

* Vídeo Explicação do Projeto:
https://youtu.be/sAe8eYXFTUc

<!-- GETTING STARTED -->
## Getting Started

Após clonar o repositório em sua máquina, siga os passos abaixo para executar a aplicação.

### Prerequisites

Para executar a aplciação Django deve-se criar uma Venv Python.

* python
  ```sh
  python -m venv venv
  ```

Em seguida, ative a venv.

* (windows)
  ```sh
  cd venv/Scripts 
  ```
* (windows)
  ```sh
  activate
  ```

### Installation

As dependencias da aplicação estão definidas no arquivo 'requirements.txt'. É necessário os instalar em sua venv.

* python
  ```sh
  pip install -r requirements.txt
  ```

Agora é só executar a aplicação.

* python
  ```sh
  python manage.py runserver
  ```



