from django.contrib.auth import get_user_model
from rest_framework.authtoken.models import Token
from rest_framework import permissions, viewsets, status
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.decorators import action
from rest_framework.permissions import IsAdminUser, IsAuthenticated, AllowAny
from rest_framework.response import Response
from .serializers import UserSerializer

class UserViewSet(viewsets.ModelViewSet):
    user = get_user_model()
    queryset = user.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated, IsAdminUser)
    # permission_classes = (AllowAny,)
    authentication_classes = (TokenAuthentication,)

    #http://localhost:8000/api/users/register/
    @action(methods=['post'], detail=False, permission_classes=[permissions.AllowAny], url_path="register")
    def register(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        user_object = get_user_model().objects.get(username=serializer.validated_data['username']) #get_user_model() pois é um custom user
        token, created = Token.objects.get_or_create(user=user_object) # quero já criar um token de acesso e retorna-lo no registro
        data = dict(serializer.data)
        data.update(dict({"token": "Token " + token.key}))
        return Response(data, status=status.HTTP_201_CREATED, headers=headers)



class TokenView(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)

        return Response(
            {'id': user.id,
             'token': "Token " + token.key,
             'email': user.email,
             'username': user.username,
             'course': user.course,
             'first_name': user.first_name,
             'last_name': user.last_name})