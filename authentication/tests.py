from django.test import TestCase
from rest_framework import status

from .models import User

class UserModelTest(TestCase):
    url = 'http://127.0.0.1:8000/api/users/register/'
    
    dicionarioUsuarioValido = {
        'email':'asdfg@gmail.com',
        'phone':'19999999999', 
        'first_name':'Jennifer', 
        'last_name':'Aniston', 
        'course':'Ciência da Computação',
        'password':'j3nn1f&r',  
        'date_joined':'2012-09-04 06:00', 
        'username': 'asdfg@gmail.com'
    }

    def post(self, dados, url=None):
        if url is None:
            url = self.url

        return self.client.post(url, dados, format='json')

   
    #CT1: Todos os campos válidos
    def test_CT1(self):
        dados = self.dicionarioUsuarioValido.copy()
        self.assertEqual((self.post(dados)).status_code, status.HTTP_201_CREATED)
        
    #CT2.1: e-mail inválido
    def test_CT21(self):
        dados = self.dicionarioUsuarioValido.copy()
        dados['email'] = 'asdfggmail.com'
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
        

    #CT2.2: e-mail não preenchido
    def test_CT22(self):
        dados = self.dicionarioUsuarioValido.copy()
        dados['email'] = ''
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
    '''    
    #CT3: e-mail já cadastrado (rodar após CT1)
    def test_CT3(self):
        dados = self.dicionarioUsuarioValido.copy()
        dados['email'] = 'asdfg@gmail.com'
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
        
        
    #CT4.1: telefone com letra
    def test_CT41(self):
        dados = self.dicionarioUsuarioValido.copy()
        dados['phone'] = '1999999999a'
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
        
        
    #CT4.2: telefone com símbolo
    def test_CT42(self):
        dados = self.dicionarioUsuarioValido.copy()
        dados['phone'] = '1999999999@'
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
    '''    
        
    #CT4.3: telefone 16 números
    def test_CT43(self):
        dados = self.dicionarioUsuarioValido.copy()
        dados['phone'] = '1999999999999999'
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
        
        
    #CT4.4: telefone não preenchido
    def test_CT44(self):
        dados = self.dicionarioUsuarioValido.copy()
        dados['phone'] = ''
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
        
    '''    
    #CT5.1: Primeiro nome possui número
    def test_CT51(self):
        dados = self.dicionarioUsuarioValido.copy()
        dados['first_name'] = 'Jenn1fer'
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
       
    #CT5.2: Primeiro nome possui símbolo
    def test_CT52(self):
        dados = self.dicionarioUsuarioValido.copy()
        dados['first_name'] = 'Jenni-fer'
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
    '''     
    #CT5.3: Primeiro nome possui 31 letras
    def test_CT53(self):
        dados = self.dicionarioUsuarioValido.copy()
        dados['first_name'] = 'Jennifer Matthew Courteney Matt'
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
        
    #CT5.4: Primeiro nome não preenchido
    def test_CT54(self):
        dados = self.dicionarioUsuarioValido.copy()
        dados['first_name'] = ''
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
    '''    
    #CT6.1: Último nome possui número
    def test_CT61(self):
        dados = self.dicionarioUsuarioValido.copy()
        dados['last_name'] = 'An1ston'
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
        
    #CT6.2: Último nome possui símbolo
    def test_CT62(self):
        dados = self.dicionarioUsuarioValido.copy()
        dados['last_name'] = 'Ani-ston'
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
        
    #CT6.3: Último nome possui 41 letras
    def test_CT63(self):
        dados = self.dicionarioUsuarioValido.copy()
        dados['last_name'] = 'Anisto Perry Cox Kudrow Schwimer LeBlanc'
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
     '''   
    
    #CT6.4: Último nome não preenchido
    def test_CT64(self):
        dados = self.dicionarioUsuarioValido.copy()
        dados['last_name'] = ''
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
        
    #CT7.1: Curso possui 31 caracteres
    def test_CT71(self):
        dados = self.dicionarioUsuarioValido.copy()
        dados['course'] = 'Ciências e Engenh da Computação'
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
        
    #CT7.2: Curso não preenchido
    def test_CT72(self):
        dados = self.dicionarioUsuarioValido.copy()
        dados['course'] = ''
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
    '''    
    #CT8.1: Senha com 16 caracteres
    def test_CT81(self):
        dados = self.dicionarioUsuarioValido.copy()
        dados['password'] = 'j3nn1f&rj3nn1f&r'
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
        
    #CT8.2: Senha com 7 caracteres
    def test_CT82(self):
        dados = self.dicionarioUsuarioValido.copy()
        dados['password'] = 'j3nn1f&'
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
    '''