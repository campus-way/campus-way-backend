from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

from authentication.api.viewsets import UserViewSet, TokenView
from group.views import GroupViewSet

router = routers.DefaultRouter()
router.register(r'users',  UserViewSet, basename="users")
router.register(r'groups', GroupViewSet, basename="groups")

urlpatterns = [
    path('api/',include(router.urls)),
    path('admin/', admin.site.urls),
    path('api/login/', TokenView.as_view()),
    path('api-auth/', include('rest_framework.urls')),
]
