# Generated by Django 3.1.7 on 2021-05-03 12:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('group', '0005_auto_20210425_2122'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='ponto_referencia',
            field=models.CharField(blank=True, max_length=40),
        ),
    ]
