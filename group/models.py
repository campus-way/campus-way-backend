from django.db import models

from authentication.models import User 


class CampoEndereco(models.Field):
    def __init__(self, max_length, null, blank, *args, **kwargs):
        self.max_length = max_length
        self.null=True
        self.blank=True
        super().__init__(*args, **kwargs)

    def db_type(self, connection):
        return 'char(%s)' % self.max_length


class Group(models.Model):
    origem = models.CharField(max_length=30)
    destino = models.CharField(max_length=30)
    endereco_origem = models.CharField(max_length=100, null=True, blank=True)
    endereco_destino = models.CharField(max_length=100, null=True, blank=True)
    ponto_referencia = models.CharField(max_length=40, blank=True)
    horario = models.DateTimeField()
    autor = models.ForeignKey(User, on_delete=models.CASCADE, related_name='groups_author')
    membros = models.ManyToManyField(User, related_name='groups_member')
    REQUIRED_FIELDS = ['destino', 'origem', 'horario', 'autor',]
    
    def __str__(self):
        # IB -> FEEC || (2021-04-26 00:31:11+00:00)
        horario = str(self.horario)[0:16].replace("-","/")
        return self.origem + " -> " + self.destino + " || (" + horario + ")"
