from django.db.models import fields
from rest_framework import serializers

from authentication.models import User
from .models import Group

class GroupSerializer(serializers.ModelSerializer):
    members_number = serializers.SerializerMethodField('members_length')

    def members_length(self, foo):
        group = Group.objects.get(id=foo.id)
        size = group.membros.all().count()
        return size

    class Meta:
        model = Group
        fields = ['id', 'origem', 'destino', 'endereco_origem',
                  'endereco_destino', 'ponto_referencia', 'horario', 'autor', 'members_number']

class MembersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ['membros']