from django.test import TestCase

from datetime import *
from django.utils import timezone
from rest_framework import status

from rest_framework.test import force_authenticate
from rest_framework.test import APIClient

from .models import Group
from authentication.models import User

class GroupModelTest(TestCase):
    url = 'http://127.0.0.1:8000/api/groups/'


    cliente = APIClient()
    user = User.objects.get(pk=3)
    cliente.force_authenticate(user, token=user.auth_token)
    
    dicionarioGrupoValido = {
        'origem':'Instituto de Computação',
        'destino':'Restaurante Universitário', 
        'horario': '2021-05-25T22:21:54-03:00',  
        'autor':3,
        'endereco_origem':'R. Saturnino de Brito', 
        'endereco_destino':'R. Sérgio Buarque de Holanda', 
        'ponto_referencia':'PB',
    }
 
    def post(self, dados, url=None):
        if url is None:
            url = self.url

        return self.cliente.post(url, dados, format='json')
    
    '''
    def test_CT1(self):
        dados = self.dicionarioGrupoValido.copy()
        self.assertEqual((self.post(dados)).status_code, status.HTTP_201_CREATED)
    '''    
        
    #CT2.1: Origem não preenchido
    def test_CT21(self):
        dados = self.dicionarioGrupoValido.copy()
        dados['origem'] = ''
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
        

    #CT2.2: Origem com 31 caracteres
    def test_CT22(self):
        dados = self.dicionarioGrupoValido.copy()
        dados['origem'] = 'Instituto Computação próximo RS'
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
        
    #CT3.1: Destino não preenchido
    def test_CT31(self):
        dados = self.dicionarioGrupoValido.copy()
        dados['destino'] = ''
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
        
        
    #CT3.2: Destino com 31 caracteres
    def test_CT32(self):
        dados = self.dicionarioGrupoValido.copy()
        dados['destino'] = 'Restaurante Universitário no PB'
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
        
        
    #CT4.1: Horário vazio
    def test_CT41(self):
        dados = self.dicionarioGrupoValido.copy()
        dados['horario'] = ''
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
        
        
    #CT4.2: Horário inválido
    def test_CT42(self):
        dados = self.dicionarioGrupoValido.copy()
        dados['horario'] = '2021-05-25'
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
        
        
    #CT4.3: Horário passado
    def test_CT43(self):
        dados = self.dicionarioGrupoValido.copy()
        dados['horario'] = '2021-05-25T22:21:54-03:00'
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
        
    '''    
    #CT5.1: Autor vazio 
    def test_CT51(self):
        dados = self.dicionarioGrupoValido.copy()
        dados['autor'] = ''
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
    '''    
    
    #CT5.2: Autor inexistente
    def test_CT52(self):
        dados = self.dicionarioGrupoValido.copy()
        dados['autor'] = -1
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
        
    #CT6: Endereço de origem com 101 caracteres
    def test_CT6(self):
        dados = self.dicionarioGrupoValido.copy()
        dados['endereco_origem'] = 'R. Saturnino de Brito, a rua do IC 3,5, bem próximo ao Restaurante Saturnino junto ao pessoal tocando'
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
        
    #CT7: Endereço de destino com 101 caracteres
    def test_CT7(self):
        dados = self.dicionarioGrupoValido.copy()
        dados['endereco_destino'] = 'R. Sérgio Buarque de Holanda a rua do PB bem próximo à Biblioteca Central e próximo ao estacionamento'
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
        
    #CT8: Ponto de referência com 41 caracteres
    def test_CT8(self):
        dados = self.dicionarioGrupoValido.copy()
        dados['ponto_referencia'] = 'Próximo à Biblioteca e ao estacionamento.'
        self.assertEqual((self.post(dados)).status_code, status.HTTP_400_BAD_REQUEST)
  