from rest_framework import serializers, viewsets, permissions
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import action

from .models import Group
from .serializers import GroupSerializer, MembersSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = (permissions.IsAuthenticated,)
    # permission_classes = (permissions.AllowAny,)


    def create(self, request, *args, **kwargs):
        """
        * A sobrescrita do metodo create é feita para assegurar que o 
        usuário que está solicitando a criação do grupo será o seu autor.
        * Se o usuário tentar colocar outro como autor do grupo, revceberá 
        uma response 400.
        * Além disso, esse método também adiciona o usuário criador do grupo
        à sua lista de membros.
        """
        # verificando se o usuario solicitando a criacao se identificou como autor
        usuario = request.user
        usuario_id = int(usuario.id)
        autor_id = int(request.data.get('autor'))
        if(usuario_id != autor_id):
            content = {'autor errado': 'o criador do grupo tem de ser seu autor'}
            return Response(content, status=status.HTTP_400_BAD_REQUEST)    
        
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        """Salvando diretamente ao inves de chamar self.perform_create(serializer) para 
        poder ter acesso ao objeto (instancia de Grupo) retornado por serializer.save()"""
        grupo = serializer.save()
        grupo.membros.add(usuario)

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


    @action(detail=True, methods=['GET', 'POST'], permission_classes=[permissions.IsAuthenticated])
    def members(self, request, pk=None):
        grupo = self.get_object()
        serializer = MembersSerializer(grupo)
        status_code = status.HTTP_200_OK
        headers = self.get_success_headers(serializer.data)
        if request.method=='POST':
            try:
                usuario = request.user
                grupo.membros.add(usuario)
                serializer = MembersSerializer(grupo)
            except:
                status_code = status.HTTP_400_BAD_REQUEST
                headers = None
        return Response(serializer.data, status=status_code, headers=headers)

    @action(detail=False, methods=['GET'], permission_classes=[permissions.IsAuthenticated])
    def my_groups(self, request, pk=None):
        """
        Metodo para retorno apenas dos grupos que o usuário faz parte
        """
        uid = request.user.id
        groups = Group.objects.filter(membros__in=[uid])
        serializer = self.get_serializer(groups, many=True)

        return Response(serializer.data, status.HTTP_200_OK)

